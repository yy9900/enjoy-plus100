Page({
  data: {
    notifyList: []
  },
  async onLoad() {
    const {
      code,
      data: notifyList
    } = await wx.http({
      url: '/announcement',
    })
    if (code !== 10000) return wx.utils.toast()
    this.setData({
      notifyList
    })
  }
})