// pages/notify/index.ts
Page({

  /**
   * 页面的初始数据
   */
  data: {
    notice: {}
  },
  async getNotice(id) {
    // 发请求
    const {
      code,
      data: notice
    } = await wx.http.get(`/announcement/${id}`)
    // 处理错误
    if (code !== 10000) return wx.utils.toast()
    // 存放数据
    this.setData({
      notice
    })
  },
  /**
   * 生命周期函数--监听页面加载,可以接受别的页面传递过来的参数
   */
  onLoad({
    id
  }) {
    this.getNotice(id)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})